using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KikiController : MonoBehaviour
{
    public const float m_maxForce = 20f;
    private Rigidbody m_rigidbodyKiki;
    void Start()
    {
        m_rigidbodyKiki = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Launch(float force)
    {
        // m_rigidbodyKiki.AddForce(new Vector3(transform.position.x, transform.position.y + 20, transform.position.z) * force);
        m_rigidbodyKiki.velocity = transform.forward;
        m_rigidbodyKiki.AddForce(Vector3.up * force, ForceMode.Impulse);
    }
    public void showForce(float force)
    {

    }

}
