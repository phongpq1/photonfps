using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MainCanvas : MonoBehaviour
{
    public GameObject btn_PressPower;
    [SerializeField] private KikiController kikiController;
    [SerializeField] private ButtonLongPressListener buttonLongPressListener;
    [SerializeField] private Slider sliderPower;
    public float m_maxForceHoldDownTime;

    void Start()
    {
        m_maxForceHoldDownTime = 2f;
        sliderPower.maxValue = m_maxForceHoldDownTime;
    }
    private void Update()
    {
        sliderPower.value = buttonLongPressListener.m_holdDownTime;
    }
    public void Press()
    {
        float m_holdDownButtonTime;
        if (buttonLongPressListener.m_holdDownTime > m_maxForceHoldDownTime)
        {
            m_holdDownButtonTime = m_maxForceHoldDownTime;
        }
        else
        {
            m_holdDownButtonTime = buttonLongPressListener.m_holdDownTime;
        }
        kikiController.Launch(HoldDownForce(m_holdDownButtonTime));
        sliderPower.value = 0;
    }
    private float HoldDownForce(float m_holdTime)
    {
        float m_holdTimeNormalized = Mathf.Clamp01(m_holdTime / m_maxForceHoldDownTime);
        float m_force = m_holdTimeNormalized * KikiController.m_maxForce;
        return m_force;
    }
}
