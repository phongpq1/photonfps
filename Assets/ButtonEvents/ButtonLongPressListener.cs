﻿/*--------------------------------------
   Email  : hamza95herbou@gmail.com
   Github : https://github.com/herbou
----------------------------------------*/

using UnityEngine ;
using UnityEngine.Events ;
using UnityEngine.EventSystems ;
using UnityEngine.UI ;

[RequireComponent (typeof(Button))]
public class ButtonLongPressListener : MonoBehaviour,IPointerDownHandler,IPointerUpHandler {

   [Tooltip ("Hold duration in seconds")]
   [Range (0.3f, 5f)] public float holdDuration = 0.5f ;
   public UnityEvent onLongPress ;

   private bool isPointerDown = false ;
   public float m_holdDownTime = 0f ;
   public float m_holdDownStartTime;
   public float m_holdTimeNormalized;

   private Button button ;

   private void Awake () {
      button = GetComponent<Button> () ;
   }

   public  void OnPointerDown (PointerEventData eventData) {
      m_holdDownTime = 0f ;
      isPointerDown = true ;
      m_holdDownStartTime = Time.time;
   }
   public  void OnPointerUp (PointerEventData eventData) {
      isPointerDown = false ;
   }

   private void Update () {
      if (isPointerDown ){
         m_holdDownTime = Time.time - m_holdDownStartTime;
      }
   }


}
